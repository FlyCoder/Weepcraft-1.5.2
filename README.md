# Weepcraft-1.5.2

Weepcraft is a minecraft hack client thats existed for ages now. The weepforums and such were taken down years ago, making weepcraft an ancient part of history. This repo allows you to download it and use it yourself!
# How to Use?

### Weepcraft isn't like majority of recent hack clients. You can't just open it and use it like that. Follow these steps on how to use it.
1. Download the jar file that listed in the repo.
2. Download MultiMC https://multimc.org/
3. When downloaded, make a 1.5.2 instance. 

![sd](https://cdn.discordapp.com/attachments/1032031995120861306/1116805521714974840/image.png)
![sd](https://media.discordapp.net/attachments/1032031995120861306/1116805553776234496/image.png)

4. When made the instance, right click it and press "Edit Instance"
5. Press version.
6. Click "Add to Minecraft Jar" and upload the jar.

![sa](https://media.discordapp.net/attachments/1032031995120861306/1116806186335993866/image.png)

7. Done! Enjoy :)